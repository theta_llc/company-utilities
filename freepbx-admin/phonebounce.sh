#/bin/bash
# @(#) File: phonebounce.sh
# ---------------------------------------------------------------------------
# Modication History:
# Date       Name                       Description
# 10/07/2018  Emmanuel Iroanya          Script to bounce our phone system
# ---------------------------------------------------------------------------

# Usage info

NGINX=/home/emmanuel/programs/nginx-1.6.0/sbin/nginx
PHP=/home/emmanuel/programs/php-5.4/sbin/php-fpm
PHP_NAME=$(basename $PHP)

startstop_service() {
    cmd=$1
    name=$2
    sudo service $name $cmd
}

startstop_nginx() {
    cmd=$1
    case $cmd in
        stop) $NGINX -s stop ;;
        start) $NGINX ;;
        restart)
            $NGINX -s stop
            sleep 1
            $NGINX
            ;;
    esac
}

startstop_php() {
    cmd=$1
    case $cmd in
        stop) pkill $PHP_NAME ;;
        start) $PHP ;;
        restart)
            pkill $PHP_NAME
            sleep 1
            $PHP
            ;;
    esac
}

case "$1" in
    start|stop|restart) cmd=$1 ;;
    *)
        shift
        servicenames=${@-servicenames}
        echo "usage: $0 [start|stop|restart] $servicenames"
        exit 1
esac
shift

for name; do
    case "$name" in
        php-fpm) startstop_php $cmd ;;
        nginx) startstop_nginx $cmd ;;
        *) startstop_service $cmd $name ;;
    esac
done