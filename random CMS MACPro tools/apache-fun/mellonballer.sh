#/bin/bash
# @(#) File: mellonballer.sh
# ---------------------------------------------------------------------------
# Modication History:
# Date         Name                       Description
# 01/30/2020   Matt Shaw                  Script to turn Mellon on/off so people can do it themselves!
#                             
# ---------------------------------------------------------------------------

# Usage info
#error codes
#exit 1 unknown failure
#exit 2 could not change direcotry
#exit 3 not bring run as root
#exit 4 unsupported argument
#exit 5 mellon config missing

ISROOT=`id -u`
MELLONDIR=/etc/httpd/conf.d/
CMD="service httpd restart"
CONF="mellon.conf"
BACK="mellon.conf.bak"
ARG=$1

checkstatus(){
    if [ -f $CONF ];then
        echo Mellon is on     
    elif [ -f $BACK ];then
        echo Mellon is off
    else
       echo config and backup missing!
       exit 5
    fi
}

sanitycheck(){
    if [ -f $CONF ] && [ "$ARG" == "on" ];then
        echo Mellon is already running! 
        exit
    elif [ -f $BACK ] && [ "$ARG" == "off" ];then
        echo Mellon is already off!
        exit
    fi

}

function checkargs(){
    case $ARG in
        on)     
            mv $BACK $CONF
            $CMD
            ;;
        off)    
            mv $CONF $BACK
            $CMD
            ;;
        status)
            checkstatus
            ;;
        *)      
            echo unsupported argument!
            exit 4
            ;;
    esac
}


if [[ $ISROOT -eq 0 ]];then
    if cd $MELLONDIR ; then
        sanitycheck
        checkargs $ARG
    else
        echo could not change directory!
        exit 2
    fi
else
    echo must be run as root!
    exit 3
fi
