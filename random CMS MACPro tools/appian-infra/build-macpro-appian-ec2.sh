#!/bin/bash
## @(#) File: build-macpro-appian-ec2.sh
# ---------------------------------------------------------------------------
# Modication History:
# Date          Name                         Description
# 03/11/2021    Mannie Iroanya               bash script for deploying MACPro Appian ec2 builds on the fly using AWS CLI - *note* this is a hack until time can be spent properly automatin' with Salt or something better
#                             
## ---------------------------------------------------------------------------

set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT

#script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd -P)

usage() {
  cat <<EOF
Usage: $(basename "${BASH_SOURCE[0]}") [-h] [-v] [-f] -p param_value arg1 [arg2...]

This is a bash script created for deploying MACPro's Appian ec2's builds on the fly. Note, this process is a Mannie "hack" until time can be spent properly automatin' with Salt or a tool that is much more scaleable/maintainable. AWS CLI mandatory on machine running this script.

Available options:

-h, --help      Print this help and exit
-v, --verbose   Print script debug info
-f, --flag      Some flag description
-p, --param     Some param description
EOF
  exit
}

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  # script cleanup here
}

setup_colors() {
  if [[ -t 2 ]] && [[ -z "${NO_COLOR-}" ]] && [[ "${TERM-}" != "dumb" ]]; then
    NOFORMAT='\033[0m' RED='\033[0;31m' GREEN='\033[0;32m' ORANGE='\033[0;33m' BLUE='\033[0;34m' PURPLE='\033[0;35m' CYAN='\033[0;36m' YELLOW='\033[1;33m'
  else
    NOFORMAT='' RED='' GREEN='' ORANGE='' BLUE='' PURPLE='' CYAN='' YELLOW=''
  fi
}

msg() {
  echo >&2 -e "${1-}"
}

die() {
  local msg=$1
  local code=${2-1} # default exit status 1
  msg "$msg"
  exit "$code"
}

parse_params() {
  # default values of variables set from params
  flag=0
  param=''

  while :; do
    case "${1-}" in
    -h | --help) usage ;;
    -v | --verbose) set -x ;;
    --no-color) NO_COLOR=1 ;;
    -f | --flag) flag=1 ;; # example flag
    -p | --param) # example named parameter
      param="${2-}"
      shift
      ;;
    -?*) die "Unknown option: $1" ;;
    *) break ;;
    esac
    shift
  done

  args=("$@")

  # check required params and arguments
  [[ -z "${param-}" ]] && die "Missing required parameter: param"
  [[ ${#args[@]} -eq 0 ]] && die "Missing script arguments"

  return 0
}

#parse_params "$@"
setup_colors

# time for some actual logic now for the script... 

instance_type=r4.2xlarge
key_pair=I2GK


type=''
environment=''
name=''
instance_exists=''
new_instance_id=''
existing_instance_id=''
existing_volume_id=''

launch_instance(){

    echo "" 
    echo "Launching EC2 virtual machine" 
    echo "- ami   : $image_id" 
    echo "- type  : $type" 
    echo "- environment: $environment"
    echo ""

    # Ask for confirmation 
    read -p "* Please confirm you want to launch an VM with these settings [y/n] " -n 1 -r
    echo ""
    if [[ ! $REPLY =~ ^[Yy]$ ]]; then echo ABORTED; exit 1; fi

    OUT=$(aws ec2 run-instances --image-id $image_id --instance-type $instance_type --iam-instance-profile "Name=instance_iam_role" --key-name $key_pair --placement \
     "AvailabilityZone=us-east-1a" --user-data file://envdata.txt --output text )
    X_ID=$(echo "$OUT" | grep INSTANCES | cut -f 9)
    X_STATE=$(echo "$OUT" | grep STATE | head -n 1 | cut -f 3) 
    echo  ""
    echo "* Instance launched >> $X_ID <<"  
    new_instance_id=$X_ID

    # tag the instance 
    aws ec2 create-tags --resources $X_ID --tags Key=Name,Value=$base_name
    aws ec2 create-tags --resources $X_ID --tags Key=Type,Value=$type

    # Wait for instance in `running` status
    while [[ $X_STATE = pending ]]; do 
        echo "- Waiting for running status"
        sleep 5
        OUT=$(aws ec2 describe-instances --instance-ids $X_ID --output text)
        X_STATE=$(echo "$OUT" | grep STATE | cut -f 3)
        echo $X_STATE   
    done


    if [[ $X_STATE != running ]]; then
    echo "* Oops .. something went wrong :("
    echo ""
    echo "$OUT"
    exit 1
    fi 

    echo "- Waiting for status checks to pass .."
    aws ec2 wait instance-status-ok --instance-ids $X_ID
    
    new_volume_id=$(aws ec2 describe-volumes --filters Name=attachment.device,Values=/dev/xvdc Name=attachment.instance-id,Values=$X_ID \
    --query 'Volumes[*].{ID:VolumeId}' --output text)

    # Fetch the publish host name   
    X_IP=$(echo "$OUT" | grep ASSOCIATION | head -n 1 | cut -f 3)

    echo "${X_IP} is created with volume ID as ${new_volume_id}"
}

check_if_instance_exists(){

    echo "- Checking if instance already exists with $base_name and $type"
    OUT=$(aws ec2 describe-instances --filters "Name=tag:Name,Values=$base_name" "Name=tag:Type,Values=$type" "Name=instance-state-name,Values=running" --output text)
    X_STATE=$(echo "$OUT" | grep STATE | cut -f 3)
    if [[ "$X_STATE" == "running" ]]; then
        instance_exists=0
        existing_instance_id=$( echo "$OUT" | grep INSTANCES |  cut -f 9)
        existing_volume_id=$(aws ec2 describe-volumes  --filters Name=attachment.device,Values=/dev/xvdc Name=attachment.instance-id,Values=$existing_instance_id \
         --query 'Volumes[*].{ID:VolumeId}' --output text)
        echo "Instance ${existing_instance_id} with ${existing_volume_id} already exists."
    else
        instance_exists=1 
        echo "- Instance doesn't exist"

    fi
}

detach_volume_from_instances(){

    echo ""
    echo "- Deattaching existing Volume ${existing_volume_id} from existing Instance ${existing_instance_id}"
    OUT=$(aws ec2 detach-volume --volume-id $existing_volume_id --output text )

    X_STATE=''

    while [[ $X_STATE != available ]]; do 
        echo "- Waiting for available status for volume"
        sleep 5
        OUT=$(aws ec2 describe-volumes  --volume-ids $existing_volume_id --output text)
        X_STATE=$(echo "$OUT" | cut -f 9)
        echo $X_STATE   
    done

    echo ""
    echo "- Deattaching New Volume from New Instance .."
    OUT=$(aws ec2 detach-volume --volume-id $new_volume_id --output text )

    X_STATE=''

    while [[ $X_STATE != available ]]; do 
        echo "- Waiting for available status for volume"
        sleep 5
        OUT=$(aws ec2 describe-volumes  --volume-ids $new_volume_id --output text)
        X_STATE=$(echo "$OUT" | cut -f 9)
        echo $X_STATE   
    done
}

stop_previous_instance(){
    echo ""
    echo "- Stopping Existing Instance $existing_instance_id .."
    X_STOPPED=''
    while [[ $X_STATE != stopped ]]; do 
        echo "- Waiting for instance stopped status"
        sleep 10
        OUT=$(aws ec2 stop-instances --instance-ids $existing_instance_id --output text)
        X_STATE=$(echo "$OUT" | grep CURRENTSTATE | cut -f 3)
        echo $X_STATE   
    done
}

attach_existing_volume(){
     X_STATE=''
     echo ""
     echo "- Attaching existing volume $existing_volume_id to new instance $new_instance_id"
     OUT=$(aws ec2 attach-volume --volume-id $existing_volume_id --instance-id $new_instance_id --device /dev/xvdc --output text )
     while [[ $X_STATE != in-use ]]; do 
        echo "- Waiting for In Use status"
        sleep 5
        OUT=$(aws ec2 describe-volumes  --volume-ids $existing_volume_id --output text)
        X_STATE=$(echo "$OUT" | grep VOLUMES | cut -f 9)
        echo $X_STATE   
    done
}

echo "Please specify the type of server "
select yn in "App" "Web"; do
    case $yn in
        App ) type=App;break;;
        Web ) type=Web;break;;
    esac
done

echo "Please specify the environment "
select yn in "Dev" "Test" "Val" "Prod"; do
    case $yn in
        Dev ) environment=Dev;break;;
        Test ) environment=Test;break;;
        Val ) environment=Val;break;;
        Prod ) environment=Prod;break;;
    esac
done

cp envdata-template.txt envdata.txt
sed -i -e "s/ENVIRONMENT/${environment}/" envdata.txt

if [ "$type" = "App" ];
then
 image_id=ami-04e44f492f7af66f6
else
 image_id=ami-0b31c692bb3eac411
fi

# echo "Please specify the base name of EC2: "  
# read base_name

base_name=appian

check_if_instance_exists $base_name $type
launch_instance $image_id $type $environment $base_name
if [[ $instance_exists == 0 ]] ;
then
   stop_previous_instance
   detach_volume_from_instances
   attach_existing_volume
   rm envdata.txt
   echo "--------------------------------------------------------------------------"
   echo "Operation Completed. Please mount volume manually by SSHing into instance."
fi

msg "${RED}Read parameters:${NOFORMAT}"
msg "- flag: ${flag}"
msg "- param: ${param}"
msg "- arguments: ${args[*]-}"