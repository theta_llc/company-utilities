#!/bin/bash
## @(#) File: cm-macpro-s3-sync.sh
# ---------------------------------------------------------------------------
# Modication History:
# Date          Name                         Description
# 05/25/2021    Mannie Iroanya               all the macpro-appian-tools that are needed, get sync'd
## ---------------------------------------------------------------------------

aws s3 sync s3://macpro-appian-tools/install-app/appian-infra/build-it/* /home/ec2-user/appian-infra/build-it/
aws s3 sync s3://macpro-appian-tools/install-app/appian-infra/snapshot-it/* /home/ec2-user/appian-infra/snapshot-it/

wall all the necessary macpro-appian-tools have been synced to this server