#!/bin/bash
# @(#) File: dnsmonitor.sh
# ---------------------------------------------------------------------------
# Modication History:
# Date          Name                    Description
# 5/4/2018      Matthew Shaw            Script to monitor DNS issues
# ---------------------------------------------------------------------------

DNS=`cat /etc/resolv.conf  |grep nameserver| cut -d' ' -f2`
PING=3
TESTHOST=macpro.cms.gov
LOG=/app/data/scripts/logs/dnscheck.log

for i in $DNS ; do
    if ! ping -c $PING $i > /dev/null; then 
        MESSAGE="`date +%b\ %d\ %T` ping check failed for $TESTHOST" 
        echo $MESSAGE >> $LOG
        echo $MESSAGE | mailx -s "Ping check failed" -r MACPRO_Operations_Team@qssinc.com \ MACPRO_Operations_Team@qssinc.com
    else
        if ! nslookup $TESTHOST $i > /dev/null ; then
            MESSAGE="`date +%b\ %d\ %T` name resolution failed for $TESTHOST" 
            echo $MESSAGE >> $LOG
            echo $MESSAGE | mailx -s "Name Resolution failed" -r MACPRO_Operations_Team@qssinc.com \ MACPRO_Operations_Team@qssinc.com
        fi
    fi
done