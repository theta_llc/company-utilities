#!/bin/bash
# @(#) File: dnsmonitor.sh
# ---------------------------------------------------------------------------
# Modication History:
# Date          Name                    Description
# 5/4/2018      Matthew Shaw            Script to monitor DNS issues
# 2/26/2019     Emmanuel Iroanya        Made edit to to and from sender in mail message 
# ---------------------------------------------------------------------------

DNS=`cat /etc/resolv.conf  |grep nameserver| cut -d' ' -f2`
PING=3
TESTHOST=macpro.cms.gov
LOG=/app/data/scripts/logs/dnscheck.log

for i in $DNS ; do
    if ! ping -c $PING $i > /dev/null; then 
        MESSAGE="`date +%b\ %d\ %T` ping check failed for $TESTHOST" 
        echo $MESSAGE >> $LOG
        echo $MESSAGE | mailx -s "Ping check failed" -r "MACPro Operations Team<macpro_operations@collabralink.com>" \ macpro_operations@collabralink.com -c emmanuel@theta-llc.com,matthew@theta-llc.com,MACPRO_Operations_Team@qssinc.com
    else
        if ! nslookup $TESTHOST $i > /dev/null ; then
            MESSAGE="`date +%b\ %d\ %T` name resolution failed for $TESTHOST" 
            echo $MESSAGE >> $LOG
            echo $MESSAGE | mailx -s "Name Resolution failed" -r "MACPro Operations Team<macpro_operations@collabralink.com>" \ macpro_operations@collabralink.com -c emmanuel@theta-llc.com,matthew@theta-llc.com,MACPRO_Operations_Team@qssinc.com
        fi
    fi
done
