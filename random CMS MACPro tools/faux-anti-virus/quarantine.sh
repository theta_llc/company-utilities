#!/bin/bash -x
# @(#) File: quarantine.sh
# ---------------------------------------------------------------------------
# Modication History:
# Date       Name           		Description
# 09/13/2016  Emmanuel Iroanya 		Script to quarintine unwanted file types in CMS MACPro Appian Product
# ---------------------------------------------------------------------------

conffile=$1

if [[ -n "$1" ]]; then
   for i in $(cat $1 | sed -ne '/DIRECTORIES/,$ p' | grep -v "DIRECTORIES" | grep -v -e '^$')
      do
        for j in $(cat $1 | sed -e '/DIRECTORIES/q' | egrep -v [DIRECTORIES,EXTENSIONS] | grep -v -e '^$')
        do
          for z in $(ls $i | grep "[.]*\\$j") 
          do mv $i/$z /app/data/tmp/quarantine/
        done
      done
   done
else
   echo "No configuration file detect"
   echo "Ex.: $ ./quarantine.sh ext.conf"
fi

if [[ -n "$1" ]]; then
  for p in $(cat $1 | sed -ne '/DIRECTORIES/,$ p' | grep -v "DIRECTORIES" | grep -v -e '^$')
  do
   for k in $(echo $p/\*/ | tr \  \\n)
   do 
     for l in $(cat $1 | sed -e '/DIRECTORIES/q' | egrep -v [DIRECTORIES,EXTENSIONS] | grep -v -e '^$')
     do 
       for x in $(ls $k | grep "[.]*\\$l")
       do mv $k/$x /app/data/tmp/quarantine/
       done
     done
   done
  done 2> /dev/null
else
   echo " "
fi