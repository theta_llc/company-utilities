#/bin/bash
## @(#) File: log_transfer.sh - Script to pull MACPro Appian logs to your localhost for review
# ---------------------------------------------------------------------------
# Modication History:
# Date        Name                               Description
# 09/23/2020  Mannie Iroanya          Script to pull MACPro Appian logs to your localhost for review
# 09/26/2020  Mannie Iroanya          Finally added VPN check, and an alert for VPN
# ---------------------------------------------------------------------------

set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT

script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd -P)

usage() {
  cat <<EOF
Usage: $(basename "${BASH_SOURCE[0]}") [-h] [-v] [-f] -p param_value arg1 [arg2...]

Script to pull MACPro Appian logs to your localhost for review, must be running *nix based OS for this to work

Available options:

-h, --help      Print this help and exit
-v, --verbose   Print script debug info
-f, --flag      Some flag description
-p, --param     Some param description
EOF
  exit
}

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  # script cleanup here
}

setup_colors() {
  if [[ -t 2 ]] && [[ -z "${NO_COLOR-}" ]] && [[ "${TERM-}" != "dumb" ]]; then
    NOFORMAT='\033[0m' RED='\033[0;31m' GREEN='\033[0;32m' ORANGE='\033[0;33m' BLUE='\033[0;34m' PURPLE='\033[0;35m' CYAN='\033[0;36m' YELLOW='\033[1;33m'
  else
    NOFORMAT='' RED='' GREEN='' ORANGE='' BLUE='' PURPLE='' CYAN='' YELLOW=''
  fi
}

msg() {
  echo >&2 -e "${1-}"
}

die() {
  local msg=$1
  local code=${2-1} # default exit status 1
  msg "$msg"
  exit "$code"
}

parse_params() {
  # default values of variables set from params
  flag=0
  param=''

  while :; do
    case "${1-}" in
    -h | --help) usage ;;
    -v | --verbose) set -x ;;
    --no-color) NO_COLOR=1 ;;
    -f | --flag) flag=1 ;; # example flag
    -p | --param) # example named parameter
      param="${2-}"
      shift
      ;;
    -?*) die "Unknown option: $1" ;;
    *) break ;;
    esac
    shift
  done

  args=("$@")

  # check required params and arguments
  [[ -z "${param-}" ]] && die "Missing required parameter: param"
  [[ ${#args[@]} -eq 0 ]] && die "Missing script arguments"

  return 0
}

parse_params "$@"
setup_colors

# the actual log transfer logic finally

transfer (){
		if [[ -f $priv_key_path ]]
    then
      scp -i $priv_key_path $remote_target:$log_path $local_store &> /dev/null 
		else
			scp $remote_target:$log_path $local_store &> /dev/null
    fi
}

check_success (){
	if [[ -f $local_store ]]
	then
		echo -e "${GREEN}Transfer succeed!${NOFORMAT}\n"
	else
		echo -e "${RED}Something went wrong, file isn't on your workstation...${NOFORMAT}\n"
	fi
}

# Check to see if the person is on CMS VPN or not, if not, I will tell you so, need to find CIDR block for CMS VPN from AWS
# ifconfig with awk made most sense

check_vpn () {
vpn=false
cidr=(10.252 10.251 10.232)
for addr in ${cidr[@]};
do
	if [ "$vpn" = false ]
	then
		vpn_ip=`ifconfig | grep -i mask | grep $addr | grep inet |awk '{print $2}'`
		if [ -n "$vpn_ip" ]
		then
			vpn=true
			break
		fi
	fi
done
}

option=0

echo -e "Welcome to Log Transfer!\n"
echo -e "Enter the Remote target name or ip:\n"
read remote_target
echo -e "\nEnter you private key path:\n"
read priv_key_path

while [ $option -ne 3 ]
do
	echo -e "\nLogs Options:\n1.Tomcat Logs\n2.Appian Engines Logs\n3.Exit"
	echo -e "Enter your option:\n"
	read option
	check_vpn
	if [ "$vpn" = true ]
		then
		if [[ $option -eq 1 ]]
		then
			log_path="/app/appian/logs/tomcat-stdOut.log"
			echo -e "Enter Where you want to store the logs on your local machine:\n" 
  		read local_store
			transfer
			check_success 
  	elif [[ $option -eq 2 ]]
		then
			log_path="/app/appian/logs/service_manager.log"
			echo -e "Enter Where you want to store the logs on your local machine:\n" 
    	read local_store
			transfer
			check_success 
		fi
	else
		echo -e "\n${RED}WARNING! You are not connected to the VPN! You can't pull Logs! Please Connect to the VPN!${NOFORMAT}\n"
	fi
done

msg "${RED}Read parameters:${NOFORMAT}"
msg "- flag: ${flag}"
msg "- param: ${param}"
msg "- arguments: ${args[*]-}"