#!/bin/bash
## @(#) File: saml-check-systemd.sh - Script to check Appian tomcat logs and look for a specific fatal SAML error
# ---------------------------------------------------------------------------
# Modication History:
# Date          Name                         Description
# 02/22/2021    Mannie Iroanya               Script to check Appian tomcat logs and look for a specific fatal SAML error
#
## ---------------------------------------------------------------------------

# logic for searching Appian tomcat logs
process_line() {
    read
    while true
    do
     #echo $REPLY
     if [[ $REPLY == *"INFO"*"Authentication Error: SAML Response"* ]]; then
        ip=$(hostname -I)
        echo "$REPLY" | mail -s "$ip" macpro_operations@collabralink.com
     fi
     read

    done
}

tail -F /app/appian/logs/tomcat-stdOut.log | process_line